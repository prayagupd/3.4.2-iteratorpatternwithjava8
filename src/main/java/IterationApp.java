import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * Created by prayagupd
 * on 4/6/15.
 */

public class IterationApp {

  public static void main(String[] args){

    List<Integer> list = new ArrayList<Integer>(){{
      add(1);
      add(2);
      add(3);
      add(4);
      add(5);
      add(6);
      add(7);
      add(8);
      add(9);
      add(10);
    }};

    System.out.println("===================== 1 =======================");
    int sum        = list.stream()
                         .mapToInt(Integer::intValue)
                         .sum();
    double average = list.stream()
                         .mapToInt(Integer::intValue)
                         .average()
                         .orElse(0);

    System.out.println("Sum = " + sum);
    System.out.println("Average = " + average);

    System.out.println("===================== 2 =======================");
    Stream<Integer> stream = list.stream()
                                 .filter(element -> element > 2);
    int sumWithPredicate = stream.mapToInt(Integer::intValue).sum();
    double averageWithPredicate = list.stream()
                                      .filter(element -> element>2)
                                      .mapToInt(Integer::intValue)
                                      .average()
                                      .orElse(0);

    System.out.println("Sum with predicate = " + sumWithPredicate);
    System.out.println("Average with predicate = " + averageWithPredicate);
  }
}
